\babel@toc {russian}{}
\contentsline {chapter}{\numberline {1}Постановка задачи}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Теория}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Распределения}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Боксплот Тьюки}{3}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Определение}{3}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Описание}{4}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Построение}{4}{subsection.2.2.3}%
\contentsline {section}{\numberline {2.3}Теоретическая вероятность выбросов}{4}{section.2.3}%
\contentsline {chapter}{\numberline {3}Реализация}{6}{chapter.3}%
\contentsline {section}{\numberline {3.1}Боксплот Тьюки}{6}{section.3.1}%
\contentsline {section}{\numberline {3.2}Доля выбросов}{9}{section.3.2}%
\contentsline {section}{\numberline {3.3}Теоретическая вероятность выбросов}{9}{section.3.3}%
\contentsline {chapter}{\numberline {4}Обсуждение}{10}{chapter.4}%
\contentsline {section}{\numberline {4.1}Доля и теоретическая вероятность выбросов}{10}{section.4.1}%
\contentsline {chapter}{\numberline {5}Литература}{11}{chapter.5}%
