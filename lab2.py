from mappings import *


def count_parameters(distribution='normal', sizes=[10, 100, 1000]):
    for size in sizes:
        avg_array = []; avg2_array = []
        med_array = []; med2_array = []
        zr_array = []; zr2_array = []
        zq_array = []; zq2_array = []
        ztr_array = []; ztr2_array = []
        for i in range(1000):
            data = data_map[distribution](size=size)
            n = len(data)

            avg = np.mean(data)
            med = np.median(data)
            zr = .5 * (data[0] + data[-1])
            zq = .5 * (data[int(n * .25)] + data[int(n * .75)])
            ztr = 2 / n * sum(data[int(.25 * n) + 1:int(.75 * n) + 1])
            avg_array.append(avg); avg2_array.append(avg ** 2)
            med_array.append(med); med2_array.append(med ** 2)
            zr_array.append(zr); zr2_array.append(zr ** 2)
            zq_array.append(zq); zq2_array.append(zq ** 2)
            ztr_array.append(ztr); ztr2_array.append(ztr ** 2)
        avg = np.mean(avg_array); avg2 = np.mean(avg2_array)
        med = np.mean(med_array); med2 = np.mean(med2_array)
        zr = np.mean(zr_array); zr2 = np.mean(zr2_array)
        zq = np.mean(zq_array); zq2 = np.mean(zq2_array)
        ztr = np.mean(ztr_array); ztr2 = np.mean(ztr2_array)
        avg_disp = avg2 - avg ** 2
        med_disp = med2 - med ** 2
        zr_disp = zr2 - zr ** 2
        zq_disp = zq2 - zq ** 2
        ztr_disp = ztr2 - ztr ** 2

        print(f'''
        [{size}]
            <avg> = {avg}
            D(avg) = {avg_disp}

            <med> = {med}
            D(med) = {med_disp}

            <zr> = {zr}
            D(zr) = {zr_disp}

            <zq> = {zq}
            D(zq) = {zq_disp}

            <ztr> = {ztr}
            D(ztr) = {ztr_disp}
        ''')