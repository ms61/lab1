\babel@toc {russian}{}
\contentsline {chapter}{\numberline {1}Постановка задачи}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Теория}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Метод максимального правдоподобия}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Оценивание параметров нормального закона распределения}{3}{section.2.2}%
\contentsline {section}{\numberline {2.3}Критерий согласия Пирсона}{4}{section.2.3}%
\contentsline {chapter}{\numberline {3}Реализация}{6}{chapter.3}%
\contentsline {section}{\numberline {3.1}Результаты}{6}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Проверка равномерно распределенной с.в. на нормальность}{6}{subsection.3.1.1}%
\contentsline {chapter}{\numberline {4}Обсуждение}{7}{chapter.4}%
\contentsline {chapter}{\numberline {5}Литература}{8}{chapter.5}%
