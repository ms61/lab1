from math import pow
from draw import Drawings
import mappings
import grabbs
import numpy as np
from matplotlib import pyplot as plt

if __name__ != '__main__':
    n = 20
    quantile = 11.07

    x = mappings.data_map['normal'](size=20)
    Drawings.draw_hist(x, bins=5)

    F = mappings.distribution_function_map['normal']
    ps = [F(-2), F(0) - F(-2),  F(2) - F(0), 1 - F(2)]
    ns = [len([el for el in x if el <= -2]), len([el for el in x if -2 < el <= 0]), len([el for el in x if 0 < el <= 2]), len([el for el in x if el > 2])]

    chi2 = sum([(ns[i] - n * ps[i]) ** 2 / n / ps[i] for i in range(4)])

    if chi2 < quantile:
        print('good')
    else:
        print('bad')

if __name__ == '__main__':
    sizes = [26]#, 20, 30]
    distr = 'uniform'
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2)

    for size in sizes:
        x = mappings.data_map[distr](size=size)# + mappings.data_map['normal'](size=n, scale=0.1)
        new_x = np.random.normal(size=4, scale=10)
        x = np.concatenate((x, new_x))
        ax1.boxplot(x)

        for i in range(20):
            x = grabbs.grabbs(x)
        ax2.boxplot(x)

        print(f'[{size}]', np.size(x))
    plt.show()
