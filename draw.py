import numpy as np
import matplotlib.pyplot as plt


class Drawings:
    @staticmethod
    def draw_function(function=lambda x: 0, xs=None, ys=None):
        if xs is None:
            xs = np.linspace(-5, 5, 100)
        if ys is None:
            ys = [function(x) for x in xs]
        plt.plot(xs, ys)

    @staticmethod
    def draw_hist(data, bins=15):
        plt.hist(data, bins=bins, density=True)
