import numpy as np
from math import sqrt
from scipy.stats import t

Gt = {
    3: 1.155,
    4: 1.496,
    5: 1.764,
    6: 1.973,
    7: 2.139,
    8: 2.274,
    9: 2.387,
    10: 2.482,
    11: 2.564,
    12: 2.636,
    13: 2.699,
    14: 2.755,
    15: 2.806,
    16: 2.852,
    17: 2.894,
    18: 2.932,
    19: 2.968,
    20: 3.001,
    21: 3.031,
    22: 3.060,
    23: 3.087,
    24: 3.112,
    25: 3.135,
    26: 3.157,
    27: 3.178,
    28: 3.199,
    29: 3.218,
    30: 3.236
}

def grabbs(x: np.ndarray):
    n = np.size(x)
    x = np.sort(x)
    mean = np.median(x)
    s = sqrt(np.var(x))

    G1 = (mean - x[0]) / s
    G2 = (x[-1] - mean) / s

    if G1 > Gt[n]:
        x = np.delete(x, 0)
    if G2 > Gt[n]:
        x = np.delete(x, -1)
    return x

