\babel@toc {russian}{}
\contentsline {chapter}{\numberline {1}Постановка задачи}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Теория}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Распределения}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Гистограмма}{3}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Определение}{3}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Графическое определение}{4}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Использование}{4}{subsection.2.2.3}%
\contentsline {chapter}{\numberline {3}Реализация}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}Гистограмма и график плотности распределения}{5}{section.3.1}%
\contentsline {chapter}{\numberline {4}Обсуждение}{7}{chapter.4}%
\contentsline {section}{\numberline {4.1}Гистограмма и график плотности распределения}{7}{section.4.1}%
\contentsline {chapter}{\numberline {5}Литература}{8}{chapter.5}%
