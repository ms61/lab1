\babel@toc {russian}{}
\contentsline {chapter}{\numberline {1}Постановка задачи}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Теория}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Распределения}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Вариационный ряд}{3}{section.2.2}%
\contentsline {section}{\numberline {2.3}Выборочные числовые характеристики}{3}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}характеристики положения}{3}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Характеристики рассеяния}{4}{subsection.2.3.2}%
\contentsline {chapter}{\numberline {3}Реализация}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}Характеристики положения и рассеяния}{5}{section.3.1}%
\contentsline {chapter}{\numberline {4}Обсуждение}{8}{chapter.4}%
\contentsline {section}{\numberline {4.1}Характеристики положения и рассеяния}{8}{section.4.1}%
\contentsline {chapter}{\numberline {5}Литература}{9}{chapter.5}%
