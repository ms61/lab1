import matplotlib.pyplot as plt
from mappings import *


# returns first and third quantile of distribution
def quantiles(distribution):
    data = np.sort(data_map[distribution](size=10000000))
    return np.quantile(data, q=0.25), np.quantile(data, q=0.75)


def boxplots(distribution, sizes=[20, 100], dirname='lab3_images', showfigs=False):
    fig = plt.figure(1, figsize=(9, 6))
    ax = fig.add_subplot(111)

    data_collection = []
    blowout_collection = []
    blowout_collection_disp = []

    q1, q3 = quantiles(distribution)
    x1 = q1 - 1.5 * (q3 - q1)
    x2 = q3 + 1.5 * (q3 - q1)
    for size in sizes:
        data = data_map[distribution](size=size)
        data_collection.append(data)

        tmp = []
        for i in range(1000):
            data = data_map[distribution](size=size)
            tmp.append((len(data[data < x1]) + len(data[data > x2])) / size)
        mean = np.mean(tmp)
        blowout_collection.append(np.mean(tmp))
        disp = 0
        for t in tmp:
            disp += (t - mean) ** 2
        disp /= len(tmp)
        blowout_collection_disp.append(disp)

    ax.boxplot(data_collection)
    if showfigs:
        plt.show()
    plt.savefig(f'{dirname}/{distribution}_boxplot.png')
    plt.clf()

    print(distribution)
    for i in range(len(sizes)):
        print(f'  [{sizes[i]}] avg blowout = {blowout_collection[i]}')
        print(f'  [{sizes[i]}] avg blowout disp = {blowout_collection_disp[i]}')

    if distribution != 'poisson':
        func = distribution_function_map[distribution]
        print(f'  Theoretical blowout: {func(x1) - func(x2) + 1}')
    else:
        func = distribution_function_map['poisson']
        print(f'  Theoretical blowout: {func(x1) - func(x2) + 1 - density_function_map["poisson"](x1)}')
