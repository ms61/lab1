\babel@toc {russian}{}
\contentsline {chapter}{\numberline {1}Постановка задачи}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Теория}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Распределения}{5}{section.2.1}%
\contentsline {section}{\numberline {2.2}Гистограмма}{5}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Определение}{5}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Графическое определение}{6}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Использование}{6}{subsection.2.2.3}%
\contentsline {section}{\numberline {2.3}Вариационный ряд}{6}{section.2.3}%
\contentsline {section}{\numberline {2.4}Выборочные числовые характеристики}{7}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}характеристики положения}{7}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Характеристики рассеяния}{7}{subsection.2.4.2}%
\contentsline {section}{\numberline {2.5}Боксплот Тьюки}{8}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}Определение}{8}{subsection.2.5.1}%
\contentsline {subsection}{\numberline {2.5.2}Описание}{8}{subsection.2.5.2}%
\contentsline {subsection}{\numberline {2.5.3}Построение}{8}{subsection.2.5.3}%
\contentsline {section}{\numberline {2.6}Теоретическая вероятность выбросов}{8}{section.2.6}%
\contentsline {section}{\numberline {2.7}Эмпирическая функция распределения}{9}{section.2.7}%
\contentsline {subsection}{\numberline {2.7.1}Статистический ряд}{9}{subsection.2.7.1}%
\contentsline {subsection}{\numberline {2.7.2}Определение}{9}{subsection.2.7.2}%
\contentsline {subsection}{\numberline {2.7.3}Описание}{10}{subsection.2.7.3}%
\contentsline {section}{\numberline {2.8}Оценки плотности вероятности}{10}{section.2.8}%
\contentsline {subsection}{\numberline {2.8.1}Определение}{10}{subsection.2.8.1}%
\contentsline {subsection}{\numberline {2.8.2}Ядерные оценки}{10}{subsection.2.8.2}%
\contentsline {chapter}{\numberline {3}Реализация}{12}{chapter.3}%
\contentsline {section}{\numberline {3.1}Гистограмма и график плотности распределения}{12}{section.3.1}%
\contentsline {section}{\numberline {3.2}Характеристики положения и рассеяния}{14}{section.3.2}%
\contentsline {section}{\numberline {3.3}Боксплот Тьюки}{16}{section.3.3}%
\contentsline {section}{\numberline {3.4}Доля выбросов}{19}{section.3.4}%
\contentsline {section}{\numberline {3.5}Теоретическая вероятность выбросов}{19}{section.3.5}%
\contentsline {section}{\numberline {3.6}Эмпирическая функция распределения}{19}{section.3.6}%
\contentsline {section}{\numberline {3.7}Ядерные оценки плотности распределения}{21}{section.3.7}%
\contentsline {chapter}{\numberline {4}Обсуждение}{26}{chapter.4}%
\contentsline {section}{\numberline {4.1}Гистограмма и график плотности распределения}{26}{section.4.1}%
\contentsline {section}{\numberline {4.2}Характеристики положения и рассеяния}{26}{section.4.2}%
\contentsline {section}{\numberline {4.3}Доля и теоретическая вероятность выбросов}{26}{section.4.3}%
\contentsline {section}{\numberline {4.4}Эмпирическая функция и ядерные оценки плотности распределения}{27}{section.4.4}%
\contentsline {chapter}{\numberline {5}Литература}{28}{chapter.5}%
