from draw import *
from mappings import *


def empiric_and_kernel(distribution, sizes=[20, 60, 100], dirname='lab4_images', showfigs=False):
    for size in sizes:
        data = data_map[distribution](size=size)
        n = size

        def empiric(x):
            return len(data[data < x]) / n

        def gaussian_kernel(u):
            return 1 / sqrt(2 * pi) * exp(-(u ** 2) / 2)

        sigma = sqrt(np.var(data))
        hn = 1.06 * sigma * n ** (-0.2)

        for multiplier in [.5, 1, 2]:
            hn = multiplier * hn
            def fn(x):
                return 1 / (n * hn) * sum([gaussian_kernel((x - xi) / hn) for xi in data])

            plt.grid()
            if distribution != 'poisson':
                Drawings.draw_function(fn, xs=np.linspace(-4, 4, 300))
                Drawings.draw_function(density_function_map[distribution], xs=np.linspace(-4, 4, 300))
            else:
                Drawings.draw_function(fn, xs=np.linspace(6, 14, 300))
                Drawings.draw_function(density_function_map[distribution], xs=np.linspace(6, 14, 300))

            plt.savefig(f'{dirname}/{distribution}{str(size)}_kernel{str(multiplier)}.png')
            if showfigs:
                plt.show()
            plt.clf()

        plt.grid()
        Drawings.draw_function(empiric, np.linspace(-10, 10, 1000))
        Drawings.draw_function(distribution_function_map[distribution], np.linspace(-10, 10, 1000))
        plt.savefig(f'{dirname}/{distribution}{str(size)}_empiric.png')
        if showfigs:
            plt.show()
        plt.clf()
