import matplotlib.pyplot as plt
from draw import Drawings
from mappings import *


def generate_and_save(dirname, distribution='normal', sizes=[10, 50, 1000], bins_map={10: 6, 50: 15, 1000: 80},
                      xs=None, showfigs=False):
    for size in sizes:
        plt.grid()

        Drawings.draw_function(density_function_map[distribution], xs)
        Drawings.draw_hist(data_map[distribution](n=size), bins=bins_map[size])

        plt.savefig(f'{dirname}/{distribution}{str(size)}.png')
        if showfigs:
            plt.show()
        plt.clf()
