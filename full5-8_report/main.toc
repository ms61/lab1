\babel@toc {russian}{}
\contentsline {chapter}{\numberline {1}Постановка задачи}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Теория}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Двумерное нормальное распределение}{5}{section.2.1}%
\contentsline {section}{\numberline {2.2}Корреляционный момент (ковариация) и коэффициент корреляции}{5}{section.2.2}%
\contentsline {section}{\numberline {2.3}Выборочные коэффициенты корреляции}{6}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Выборочный коэффициент корреляции Пирсона}{6}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Выборочный квадрантный коэффициент корреляции}{6}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Выборочный коэффициент ранговой корреляции Спирмена}{6}{subsection.2.3.3}%
\contentsline {section}{\numberline {2.4}Регрессионная модель описания данных}{7}{section.2.4}%
\contentsline {section}{\numberline {2.5}Метод наименьших квадратов}{8}{section.2.5}%
\contentsline {section}{\numberline {2.6}Метод наименьишх модулей (МНМ)}{8}{section.2.6}%
\contentsline {section}{\numberline {2.7}Метод максимального правдоподобия}{8}{section.2.7}%
\contentsline {section}{\numberline {2.8}Оценивание параметров нормального закона распределения}{9}{section.2.8}%
\contentsline {section}{\numberline {2.9}Критерий согласия Пирсона}{9}{section.2.9}%
\contentsline {section}{\numberline {2.10}Доверительные интервалы для $m$ и $\sigma $}{10}{section.2.10}%
\contentsline {chapter}{\numberline {3}Реализация}{12}{chapter.3}%
\contentsline {section}{\numberline {3.1}Результаты}{12}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Двумерное нормальное распределение}{12}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Смесь нормальных распределений}{16}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}Простая линейная регрессия}{17}{section.3.2}%
\contentsline {section}{\numberline {3.3}Проверка гипотезы о законе распределения генеральной совокупности}{18}{section.3.3}%
\contentsline {section}{\numberline {3.4}Доверительные интервалы для $m$ и $\sigma $}{19}{section.3.4}%
\contentsline {chapter}{\numberline {4}Обсуждение}{20}{chapter.4}%
\contentsline {section}{\numberline {4.1}Двумерное нормальное распределение}{20}{section.4.1}%
\contentsline {section}{\numberline {4.2}Простая линейная регрессия}{20}{section.4.2}%
\contentsline {section}{\numberline {4.3}Проверка гипотезы о законе распределения генеральной совокупности}{21}{section.4.3}%
\contentsline {section}{\numberline {4.4}Доверительные интервалы $m$ и $\sigma $}{21}{section.4.4}%
\contentsline {chapter}{\numberline {5}Литература}{22}{chapter.5}%
