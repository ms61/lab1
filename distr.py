from math import *


class Densities:

    @staticmethod
    def normal_function(x):
        return 1 / sqrt(2 * pi) * exp((-x ** 2) / 2)

    @staticmethod
    def cauchy_function(x):
        return 1 / pi / (1 + x ** 2)

    @staticmethod
    def laplace_function(x):
        return sqrt(2) / 2 * exp(-abs(x) * sqrt(2))

    @staticmethod
    def uniform_function(x):
        if -sqrt(3) <= x <= sqrt(3):
            return .5 * sqrt(3)
        else:
            return 0

    @staticmethod
    def poisson_function(k):
        k = int(k)
        return (10 ** k) / factorial(k) * exp(-10)


class Distributions:

    @staticmethod
    def normal(x):
        return .5 * (1 + erf(x / sqrt(2)))

    @staticmethod
    def cauchy(x):
        return 1 / pi * atan(x) + .5

    @staticmethod
    def laplace(x):
        if x <= 1 / sqrt(2):
            return .5 * exp(sqrt(2) * x)
        else:
            return 1 - .5 * exp(-sqrt(2) * x)

    @staticmethod
    def uniform(x):
        if x < -sqrt(3):
            return 0
        elif x > sqrt(3):
            return 1
        else:
            return (x - (-sqrt(3))) / 2 / sqrt(3)

    @staticmethod
    def poisson(k):
        sum = 0
        for i in range(int(k) + 1):
            sum += Densities.poisson_function(i)
        return sum

