\babel@toc {russian}{}
\contentsline {chapter}{\numberline {1}Постановка задачи}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Теория}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Двумерное нормальное распределение}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Корреляционный момент (ковариация) и коэффициент корреляции}{3}{section.2.2}%
\contentsline {section}{\numberline {2.3}Выборочные коэффициенты корреляции}{4}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Выборочный коэффициент корреляции Пирсона}{4}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Выборочный квадрантный коэффициент корреляции}{4}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Выборочный коэффициент ранговой корреляции Спирмена}{4}{subsection.2.3.3}%
\contentsline {chapter}{\numberline {3}Реализация}{6}{chapter.3}%
\contentsline {section}{\numberline {3.1}Результаты}{6}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Двумерное нормальное распределение}{6}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Смесь нормальных распределений}{10}{subsection.3.1.2}%
\contentsline {chapter}{\numberline {4}Обсуждение}{11}{chapter.4}%
\contentsline {chapter}{\numberline {5}Литература}{12}{chapter.5}%
