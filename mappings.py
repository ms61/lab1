from distr import Densities
from distr import Distributions
import numpy as np
from math import *

density_function_map = {
    'normal': Densities.normal_function,
    'cauchy': Densities.cauchy_function,
    'laplace': Densities.laplace_function,
    'uniform': Densities.uniform_function,
    'poisson': Densities.poisson_function
}
distribution_function_map = {
    'normal': Distributions.normal,
    'cauchy': Distributions.cauchy,
    'laplace': Distributions.laplace,
    'uniform': Distributions.uniform,
    'poisson': Distributions.poisson
}
data_map = {
    'normal': np.random.normal,
    'cauchy': np.random.standard_cauchy,
    'laplace': lambda size: np.random.laplace(scale=(1 / sqrt(2)), size=size),
    'uniform': lambda size: np.random.uniform(-sqrt(3), sqrt(3), size=size),
    'poisson': lambda size: np.random.poisson(10, size=size)
}